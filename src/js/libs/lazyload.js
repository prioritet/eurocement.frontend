import LazyLoad from 'vanilla-lazyload';
import fixObjectFit from '../utils/fixObjectFit';

const lazyload = {
	init() {
		const lazy = new LazyLoad({
			elements_selector: '.is-lazy',
			class_loading: 'is-loading',
			class_loaded: 'is-loaded',
			class_error: 'is-error',
			callback_loaded: this.fixObjectFit,
		});
		lazy.update();

		window.addEventListener('init.lazyload', () => {
			lazy.update();
		});
	},

	// Для исправления отображения изображений со свойствами object-fit в браузерах без их поддержки
	fixObjectFit: (el) => {
		fixObjectFit(el);
	},
};

export default lazyload;
