import IMask from 'imask';

const mask = {
	init() {
		this.tel();
		this.email();
	},
	email() {
		const elements = document.querySelectorAll('.js-mask-email, [data-mask-email]');
		const options = {
			mask: /^[a-zA-Z0-9@._-]+$/,
		};
		if (elements.length) {
			elements.forEach((el) => {
				if (el.hasAttribute('data-initialized-mask')) return;
				// eslint-disable-next-line no-unused-vars
				const maskInstance = IMask(el, options);
				el.setAttribute('data-initialized-mask', '');
			});
		}
	},
	tel() {
		const elements = document.querySelectorAll('.js-mask-tel, [data-mask-tel]');

		const maskOptions = {
			mask: '+{7}(000)000-00-00',
		};

		elements.forEach((el) => {
			if (el.hasAttribute('data-initialized-mask')) return;
			// eslint-disable-next-line no-unused-vars
			let maskInstanse = IMask(el, maskOptions);
			el.setAttribute('data-initialized-mask', '');
			el.addEventListener('mask-init', () => {
				if (!el.hasAttribute('data-initialized-mask')) {
					maskInstanse = IMask(el, maskOptions);
					el.setAttribute('data-initialized-mask', '');
				}
			});
			el.addEventListener('mask-destroy', () => {
				if (el.hasAttribute('data-initialized-mask')) {
					maskInstanse.destroy();
					el.removeAttribute('data-initialized-mask');
				}
			});
		});
	},
};

export default mask;
