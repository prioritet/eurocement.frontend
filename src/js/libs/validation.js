import 'parsleyjs';
import 'parsleyjs/src/i18n/ru.js';

$(document)
	.ready(() => {
		const $lang = $('html')
			.attr('lang');
		const $forms = $('[data-parsley-validate]');
		$forms.each((index, form) => {
			const $form = $(form);
			$form.parsley();
			window.Parsley.setLocale(`${$lang}`);
		});
	});
