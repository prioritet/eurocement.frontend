require('select2/dist/js/select2.full.js');
import PerfectScrollbar from 'perfect-scrollbar';

const select = {
	init() {
		this.initSelect();
		window.addEventListener('init.select', () => {
			this.initSelect();
		});
	},
	initSelect() {
		const elements = document.querySelectorAll('[data-select]');
		elements.forEach((select) => {
			if (select.initialized) return;
			const $select = $(select);
			const size = $select.data('select-size') || 'default';
			const placeholder = $select.data('select-placeholder') || false;
			// const dropdownParent = $select.closest('.field') || $(document.body);
			let search;
			if ($select.data('select-search')) {
				search = 0
			} else {
				search = Infinity
			}

			// Icon class
			let icon = $select.data('select-icon');
			let iconClass = '';

			if(icon) {
				iconClass = ' select2--icon-' + icon;
			}

			$select.select2({
				width: $select.data('width') || '100%',
				theme: $select.data('select-theme') || 'white',
				containerCssClass: 'select2-selection--' + size + iconClass,
				dropdownCssClass: 'select2-dropdown--' + size,
				placeholder,
				// dropdownParent,
				minimumResultsForSearch: search,
				language: {
					noResults: () => 'Совпадений не найдено',
				},
			});

			let ps;

			$select.on('select2:open', () => {
				setTimeout(() => {
					const content = document.querySelector('.select2-results__options');
					ps = new PerfectScrollbar(content, {
						suppressScrollX: true, // disable scrollX
					});
				}, 0)
			});

			$select.on('select2:closing', () => {
				ps.destroy();
			});

			select.initialized = true;
		});
	},
};

export default select;
