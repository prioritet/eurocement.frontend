import './polyfills';
import './utils/throttle';
import './utils/slideToggle';
import './utils/scroll';
import './utils/userAgent';
import './utils/transitionDealy';
import './utils/vhDefinition';

import './libs';

import { devices } from 'Utils/breakpoints';

import Tabs from './components/tabs';
import Video from './components/video';
import ButtonCheckbox from './components/button-checkbox';
import Filter from './components/filter';
import ShowMore from './components/show-more';
import Parallax from './components/parallax';
import Header from './components/header';
import SearchBar from './components/search-bar';
import SearchBox from './components/search-box';
import StickySidebar from 'sticky-sidebar';
import './components/accordion';
import './components/popup';
import './components/filter-expand';
import './components/filter-tags';
import 'focus-visible';

window.addEventListener('load', () => {
	document.documentElement.classList.add('is-loaded');
});

document.addEventListener('DOMContentLoaded', () => {
	new Video();
	Header.init();
	SearchBar.init();
	SearchBox.init();
});

window.UPB = window.UPB || {};
window.breakpoints = devices;

require('./components/slider/index');
require('./components/tooltip/index');
require('./components/fields/field-file');
require('./components/fields/field-checkbox');
require('./components/modal/modal-form');
require('./components/to-bottom-button/index');
require('./components/preloader/preloader');

window.Tabs = Tabs;
window.ButtonCheckbox = ButtonCheckbox;

function findIndex(el) {
	const items = el.querySelectorAll('.swiper-slide');
	for (let i = 0; i < items.length; i++) {
		if (items[i].classList.contains('active')) {
			return i;
		}
	}
	return false;
}

// Tabs
new Tabs({
	init: true,
	wrap: '[data-tabs]',
	tab: '.tabs__tab',
	content: '[data-content]',
	activeClass: 'active',
	underline: '.tabs__underline',
});

// Show more link
new ShowMore({
	linkWrapSelector: '.quote-block',
	linkClass: 'js-show-more'
});

// Parallax image hero
new Parallax({
	selector: '[data-parallax-hero]',
	center: false
});

// Parallax image banner
new Parallax({
	selector: '[data-parallax-banner]',
	center: true,
});
new Filter();

// Sticky block
document.addEventListener('DOMContentLoaded', () => {
	if(!document.querySelector('.vacancy-detail-block__wrap') || window.innerWidth < devices.lg) return;

	new StickySidebar('.vacancy-detail-block__wrap', {
		containerSelector: '.vacancy-detail-block',
		innerWrapperSelector: '.vacancy-detail-block__wrap-inner',
	});
});
//select2 position
(function($) {

	var Defaults = $.fn.select2.amd.require('select2/defaults');

	$.extend(Defaults.defaults, {
		dropdownPosition: 'auto'
	});

	var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');

	var _positionDropdown = AttachBody.prototype._positionDropdown;

	AttachBody.prototype._positionDropdown = function() {

		var $window = $(window);

		var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
		var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

		var newDirection = null;

		var offset = this.$container.offset();

		offset.bottom = offset.top + this.$container.outerHeight(false);

		var container = {
			height: this.$container.outerHeight(false)
		};

		container.top = offset.top;
		container.bottom = offset.top + container.height;

		var dropdown = {
			height: this.$dropdown.outerHeight(false)
		};

		var viewport = {
			top: $window.scrollTop(),
			bottom: $window.scrollTop() + $window.height()
		};

		var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
		var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

		var css = {
			left: offset.left,
			top: container.bottom
		};

		// Determine what the parent element is to use for calciulating the offset
		var $offsetParent = this.$dropdownParent;

		// For statically positoned elements, we need to get the element
		// that is determining the offset
		if ($offsetParent.css('position') === 'static') {
			$offsetParent = $offsetParent.offsetParent();
		}

		var parentOffset = $offsetParent.offset();

		css.top -= parentOffset.top
		css.left -= parentOffset.left;

		var dropdownPositionOption = this.options.get('dropdownPosition');

		if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {
			newDirection = dropdownPositionOption;
		} else {

			if (!isCurrentlyAbove && !isCurrentlyBelow) {
				newDirection = 'below';
			}

			if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
				newDirection = 'above';
			} else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
				newDirection = 'below';
			}

		}

		if (newDirection == 'above' ||
			(isCurrentlyAbove && newDirection !== 'below')) {
			css.top = container.top - parentOffset.top - dropdown.height;
		}

		if (newDirection != null) {
			this.$dropdown
				.removeClass('select2-dropdown--below select2-dropdown--above')
				.addClass('select2-dropdown--' + newDirection);
			this.$container
				.removeClass('select2-container--below select2-container--above')
				.addClass('select2-container--' + newDirection);
		}

		this.$dropdownContainer.css(css);

	};

})(window.jQuery);
