import gsap from 'gsap';
import { isDesktop, isTouch } from 'Utils/breakpoints';

const functionCursor = () => {
	const cursor = document.querySelector('.cursor');
	if (!cursor) return;
	if (isTouch()) return;
	document.documentElement.classList.add('cursor-custom');
	const border = cursor.querySelector('.cursor-border');
	gsap.set(cursor, {
		xPercent: -50,
		yPercent: -50,
	});
	if (cursor) {
		document.addEventListener('pointermove', movecursor);
		document.addEventListener('click', borderScale);
	} else {
		document.removeEventListener('pointermove', movecursor);
		document.removeEventListener('click', borderScale);
	}

	function borderScale() {
		gsap.to(border, {
			opacity: .4,
			scale: 2.5,
			duration: .1,
			onComplete: () => {
				gsap.to(border, {
					opacity: 0,
					scale: 1,
					duration: .2,
					ease: 'power1',
				});
			},
		});
	}

	function movecursor(e) {
		const target = e.target;
		if (target.closest('.bg-primary') || target.closest('.btn--hover-primary')
			|| target.closest('.checkbox__box')) {
			cursor.classList.add('bg-white');
			border.classList.add('bg-white');
		} else {
			cursor.classList.remove('bg-white');
			border.classList.remove('bg-white');
		}
		cursor.style.display = 'flex';
		gsap.to(cursor, {
			x: e.clientX,
			y: e.clientY,
		});
	}
};

functionCursor();

window.addEventListener('resize', window._throttle(() => {
	functionCursor();
}, 300), {
	passive: true,
});
