import Plyr from 'plyr';

export default class Video {
	constructor() {
		this.init();
	}

	init() {
		const videos = document.querySelectorAll('[data-player-video]');
		videos.forEach((el) => {
			const parent = el.closest('.video');
			if (parent && parent.classList.contains('init')) return;

			const poster = el.getAttribute('poster') || el.getAttribute('data-poster');
			const player = new Plyr(el, {
				poster: poster || false,
				fullscreen: {
					// iosNative: true,
					fallback: true,
				},
			});

			const parentSlide = parent.closest('.swiper-slide');
			if (parentSlide) {
				player.on('play', () => {
					parentSlide.classList.add('video-play');
				});
				player.on('pause', () => {
					parentSlide.classList.remove('video-play');
				});
			}

			window.addEventListener('mediaSlideChange', () => {
				player.pause();
			})
		});
	}
}
