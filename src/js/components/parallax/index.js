import Rellax from 'rellax';

export default class Parallax {
	constructor(props) {
		this.selector = props.selector;
		this.center = props.center;

		this.init();
	}

	init() {
		const parallaxEls = document.querySelectorAll(this.selector + ' img');

		if (!parallaxEls.length) return;

		// TODO: destroy if not in viewport
		parallaxEls.forEach(el => {
			var rellax = new Rellax(el, {
				speed: -2,
				center: this.center,
				wrapper: null,
				// wrapper: wrap,
				round: false,
				vertical: true,
				horizontal: false,
			});
			window.addEventListener('modal:open', () => {
				setTimeout(() => {
					el.style = '';
				}, 2);
			});
			window.addEventListener('rellax:refresh', () => {
				if (rellax) {
					rellax.refresh();
				}
			});
			window.addEventListener('toggleAccordion', () => {
				if (rellax) {
					rellax.refresh();
				}
			});
			window.addEventListener('resize', window._throttle(() => {
				if (rellax) {
					rellax.refresh();
				}
			}, 300), {
				passive: true,
			});
		});
	}
}
