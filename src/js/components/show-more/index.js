export default class ShowMore {
	constructor(props) {
		this.linkWrapSelector = props.linkWrapSelector;
		this.linkClass = props.linkClass;
		this.default = {};
		this.more = {};
		this.beforeText = '';
		this.afterText = '';
		this.duration = props.duration || 300;

		this.init()
	}

	setStyles(el) {
		el.style.transitionProperty = 'height';
		el.style.transitionDuration = this.duration + 'ms';
	}

	removeStyles(el) {
		setTimeout(() => {
			el.style.removeProperty('height');
			el.style.removeProperty('transition-duration');
			el.style.removeProperty('transition-property');
		}, this.duration);
	}

	toggleHandler(el) {
		let itemWrap;

		this.default = el.parentElement.querySelector('.quote-block__text');
		this.more = el.parentElement.querySelector('.quote-block__text--more');

		// Get link text
		this.beforeText = el.dataset.before;
		this.afterText = el.dataset.after;

		itemWrap = this.more.parentElement;

		// Start height
		itemWrap.style.height = itemWrap.offsetHeight + 'px';

		this.setStyles(itemWrap);

		this.more.classList.toggle('disable');
		this.default.classList.toggle('disable');

		if(!el.classList.contains('active')){
			itemWrap.style.height = this.more.offsetHeight + 'px';
			el.innerHTML = this.afterText;
		} else {
			itemWrap.style.height = this.default.offsetHeight + 'px';
			el.innerHTML = this.beforeText;
		}

		this.removeStyles(itemWrap);

		el.classList.toggle('active');
	}

	init() {
		let linksWrap = document.querySelectorAll(this.linkWrapSelector);

		if(linksWrap.length === 0) return;

		linksWrap.forEach(el => {
			el.addEventListener('click', e => {
				if(e.target.classList.contains(this.linkClass)){
					this.toggleHandler(e.target);
				}
			});
		});
	}
}
