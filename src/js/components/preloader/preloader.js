import { CountUp } from 'countup.js';
import gsap from 'gsap';

const preloader = document.querySelector('#preloader');
const body = document.querySelector('body');
const hidePreloaderAfter = 4300;
const preloaderOffset = 4300;
let animationTime = 3000;
let isLoaded = false;
let isReady = false;

if (preloader) {
	const circle = preloader.querySelector('.preloader__circle');
	const circleStrip = circle.querySelector('circle');
	const counter = preloader.querySelector('.preloader__counter');
	const counterNumbers = preloader.querySelectorAll('[data-preloader-counter-number]');


	const tl = gsap.timeline();
	let index = 0;

	gsap.to(circleStrip, {
		strokeDashoffset: 0,
		duration: 5,
		ease: 'power4.easeOut',
	});
	const t2 = gsap.timeline();
	const duration = 0.5;
	const preloaderInit = (i) => {
		gsap.fromTo(counterNumbers[i], {
			opacity: 0,
			translateY: 8,
		}, {
			opacity: 1,
			translateY: -8,
			ease: 'power4.easeOut',
			duration,
			onComplete: function () {
				const j = i + 1;
				const current = counterNumbers[i];
				const next = counterNumbers[j];
				if (next) {
					i++;
					t2.fromTo(current, {
						opacity: 1,
					}, {
						opacity: 0,
						duration: .1,
						ease: 'power4.easeOut',
					})
						.fromTo(next, {
							opacity: 0,
							translateY: 8,
						}, {
							opacity: 1,
							translateY: -8,
							duration,
							ease: 'power4.easeOut',
						}, .1);
					preloaderInit(i);
				} else {
					tl.fromTo(counter, {
						opacity: 1,
					}, {
						duration: .3,
						opacity: 0,
					})
						.to(circle, {
							opacity: 0,
							rotate: 45,
							duration: .3,
							onComplete: () => {
								gsap.to(preloader, {
									translateX: '100%',
									duration: .7,
									ease: 'power4.easeOut',
								});
							},
						});
				}
			},
		});
	};
	preloaderInit(index);

	window.addEventListener('preloader:hide', () => {
		clearTimeout(window.__preloaderTimer);
		clearTimeout(window.__preloaderCloseTimer);
		preloader.classList.add('hidden');

		setTimeout(() => {
			preloader.classList.add('d-none');
		}, animationTime);
	});

	window.addEventListener('preloader:show', () => {

		clearTimeout(window.__preloaderTimer);
		clearTimeout(window.__preloaderCloseTimer);

		preloader.classList.remove('d-none');

		window.__preloaderTimer = setTimeout(() => {
			preloader.classList.remove('hidden');
			window.__preloaderCloseTimer = setTimeout(() => {
				window.dispatchEvent(new CustomEvent('preloader:hide'));
			}, hidePreloaderAfter);
		}, preloaderOffset);
	});

	document.addEventListener('DOMContentLoaded', () => {
		isLoaded = true;
		body.classList.add('is-loaded');

		setTimeout(() => {
			window.dispatchEvent(new CustomEvent('preloader:hide'));
			setTimeout(() => {
				isReady = true;
				body.classList.add('is-ready');
			}, animationTime);
		}, preloaderOffset);
	});
}
