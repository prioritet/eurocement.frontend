import 'magnific-popup';
import PerfectScrollbar from 'perfect-scrollbar';

$(document)
	.ready(() => {
		const buttons = $('[data-modal]');
		let timer = null;
		buttons.each((index, btn) => {
			const $btn = $(btn);
			$btn.magnificPopup({
				preloader: false,
				removalDelay: 500, //delay removal by X to allow out-animation
				callbacks: {
					beforeOpen: function () {
						this.st.mainClass = this.st.el.attr('data-effect');
						this.toggleText = this.st.el.closest('[data-toggle-text]')
							.attr('data-toggle-text');
						if (this.toggleText) {
							this.text = this.st.el.closest('[data-toggle-text]')
								.find('[data-change-text]')
								.html();
						}
						window.dispatchEvent(new CustomEvent('modal:open'));
					},
					open: function () {
						window._disableScroll();
						this.st.el.removeClass('modal-open');
						this.st.el.addClass('modal-open');
						const vacancyId = $(this.st.el)[0].hasAttribute('data-vacancy-id');
						const modal = document.querySelector('.modal');
						const select = modal.querySelector('.select2');
						if (vacancyId && select) {
							select.classList.add('select-disabled');
						} else if (select) {
							select.classList.remove('select-disabled');
						}
						if (modal.classList.contains('modal--message')) {
							timer = setTimeout(() => {
								$.magnificPopup.close();
								timer = null;
							}, 5000);
						}
						const wrap = document.querySelector('.modal__content div');
						setTimeout(() => {
							this.ps = new PerfectScrollbar(wrap, {
								suppressScrollX: true,
								wheelPropagation: true,
							});
						});
						if (this.toggleText) {
							this.st.el.closest('[data-toggle-text]')
								.find('[data-change-text]')
								.html(this.toggleText);
						}
						setTimeout(() => {
							this.ev[0].classList.add('active');
						}, 50);
					},
					close: function () {
						if (timer) {
							timer = null;
						}
						if (this.ps) {
							this.ps.destroy();
						}
						$('.modal form [data-select]')
							.val(null)
							.trigger('change');
						$('input[data-vacancy-name-form]')
							.val('');
						$('h3.modal__head.h3')
							.find('span')
							.html('');
						window._enableScroll();
						this.st.el.removeClass('modal-open');
						if (this.toggleText) {
							this.st.el.closest('[data-toggle-text]')
								.find('[data-change-text]')
								.html(this.text);
						}
						this.ev[0].classList.remove('active');
					},
				},
			});
			$btn.on('click', () => {
				if ($btn.hasClass('active')) {
					setTimeout(() => {
						$.magnificPopup.close();
					}, 50);
				}
			});
		});
	});
