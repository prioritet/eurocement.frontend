export default class ButtonCheckbox {
	constructor(props) {
		this.init = props.init;
		this.wrap = props.wrap;
		this.button = props.button;
		this.input = props.input;
		this.toggleClass = props.toggleClass;

		if (this.init) {
			this.render();
		}
	}

	render() {
		const wrapps = document.querySelectorAll(this.wrap);

		for (let i = 0; i < wrapps.length; i++) {
			const button = wrapps[i].querySelector(this.button);
			const input = wrapps[i].querySelector(this.input);

			button.addEventListener('click', () => {
				button.classList.toggle(this.toggleClass);
				input.checked = !input.checked;
			});
		}
	}
}
