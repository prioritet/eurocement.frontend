document.addEventListener('DOMContentLoaded', () => {
	const checkboxFields = document.querySelectorAll('.checkbox');
	if (checkboxFields.length) {
		checkboxFields.forEach(field => {
			const input = field.querySelector('input');
			field.addEventListener('click', () => {
				setTimeout(() => {
					if (input.checked === false) {
						input.classList.add('blur');
					} else {
						input.classList.remove('blur');
					}
				}, 50);
			});
		});
	}
});
