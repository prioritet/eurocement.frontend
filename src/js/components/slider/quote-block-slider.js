import Slider from './constructor';

// Quote sliders
new Slider({
	init: true,
	wrap: '[data-slider-wrap]',
	slider: '[data-slider]',
	prev: '[data-nav-arrow-prev]',
	next: '[data-nav-arrow-next]',
	options: {
		slidesPerView: 1,
		spaceBetween: 16,
		allowTouchMove: true,
		speed: 500,
		effect: 'fade',
		on: {
			slideNextTransitionStart: function () {
				const wrap = this.el.closest('.quote-block');
				if (wrap) {
					const title = wrap.querySelector('.quote-block__title-main');
					if (title) {
						title.style.clipPath = 'initial';
					}
					wrap.classList.remove('in-viewport');
				}
				if (this.activeIndex - 1) {
					const prev = this.slides[this.activeIndex - 1];
					const container = prev.querySelector('.quote-block__container');
					container.classList.remove('in-viewport');
				} else {
					const prev = this.slides[0];
					const container = prev.querySelector('.quote-block__container');
					container.classList.remove('in-viewport');
				}
			},
			slidePrevTransitionStart: function () {
				if (this.activeIndex + 1) {
					const prev = this.slides[this.activeIndex + 1];
					const container = prev.querySelector('.quote-block__container');
					container.classList.remove('in-viewport');
				} else {
					const prev = this.slides[this.slides.length - 1];
					const container = prev.querySelector('.quote-block__container');
					container.classList.remove('in-viewport');
				}
			},
			slideChangeTransitionEnd: function () {
				const current = this.slides[this.activeIndex];
				const container = current.querySelector('.quote-block__container');
				container.classList.add('in-viewport');
			},
		},
	},
});
