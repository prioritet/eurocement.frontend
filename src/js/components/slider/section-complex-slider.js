import Slider from './constructor';
import gsap from 'gsap';

const slider = document.querySelector('[data-section-complex-slider]');
let loop = false;
if (slider) {
	const slides = slider.querySelectorAll('.swiper-slide');
	if (slides.length > 1) {
		loop = true;
	}
}

const sectionComplexSlider = new Slider({
	init: true,
	wrap: '[data-section-complex-slider-wrap]',
	slider: '[data-section-complex-slider]',
	prev: '[data-nav-arrow-prev]',
	next: '[data-nav-arrow-next]',
	options: {
		slidesPerView: 1,
		spaceBetween: 24,
		allowTouchMove: true,
		a11y: false,
		speed: 700,
		effect: 'fade',
		roundLengths: true,
		observeParents: true,
		observer: true,
		breakpoints: {
			[window.breakpoints.md]: {
				allowTouchMove: true,
			},
			[window.breakpoints.lg]: {
				allowTouchMove: false,
			},
		},
		on: {
			slideNextTransitionStart() {
				const swiper = this;
				const activeIndex = swiper.activeIndex - 1 > 0 ? swiper.activeIndex - 1 : 0;
				const activeSlide = swiper.slides[swiper.activeIndex];
				const prevActiveSlide = swiper.slides[activeIndex];

				const prevContent = prevActiveSlide.querySelector('.section-complex-slider__item');
				const content = activeSlide.querySelector('.section-complex-slider__item');

				const t2 = gsap.timeline();
				t2.to(prevContent, {
					opacity: 0,
					duration: .5,
					ease: 'power4.easeOut',
				})
					.fromTo(content, {opacity: 0, translateY: 20}, {
						opacity: 1,
						translateY: 0,
						duration: .7,
						ease: 'power4.easeOut',
					});
			},
			slidePrevTransitionStart() {
				const swiper = this;
				const activeIndex = swiper.activeIndex - 1 > 0 ? swiper.activeIndex - 1 : 0;
				const activeSlide = swiper.slides[swiper.activeIndex];
				const prevActiveSlide = swiper.slides[activeIndex];

				const prevContent = prevActiveSlide.querySelector('.section-complex-slider__item');
				const content = activeSlide.querySelector('.section-complex-slider__item');

				const t2 = gsap.timeline();
				t2.to(prevContent, {
					opacity: 0,
					duration: .5,
					ease: 'power4.easeOut',
				})
					.fromTo(content, {translateY: 20}, {
						opacity: 1,
						translateY: 0,
						duration: 0.7,
						ease: 'power4.easeOut',
					}, .7);
			},
		},
	},
});
