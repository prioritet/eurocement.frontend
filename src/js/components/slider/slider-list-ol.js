import Slider from './constructor';

const sliderList = new Slider({
	init: true,
	wrap: '[data-slider-list-ol-wrap]',
	slider: '[data-slider-list-ol]',
	prev: '[data-nav-arrow-prev]',
	next: '[data-nav-arrow-next]',
	options: {
		slidesPerView: 1,
		spaceBetween: 16,
		slidesPerColumn: 1,
		slidesPerColumnFill: 'column',
		roundLengths: true,
		allowTouchMove: true,
		a11y: false,
		speed: 700,
		observeParents: true,
		observer: true,
		breakpoints: {
			[window.breakpoints.md]: {
				allowTouchMove: true,
				spaceBetween: 2,
				slidesPerColumn: 2,
				slidesPerView: 1,
			},
			[window.breakpoints.lg]: {
				allowTouchMove: true,
				spaceBetween: 2,
				slidesPerColumn: 1,
				slidesPerView: 2,
			},
		},
	},
});
