export default class Filter {
	constructor() {
		const filters = document.querySelectorAll('.filter');
		this.tempValues = [];
		if (filters.length) {
			filters.forEach(filter => {
				this.tagWrap = filter.querySelector('.filter-tags__wrap');
				this.init(filter);
			});
		}
	}

	init(filter) {
		this.clearButtons = filter.querySelectorAll('[data-clear]');
		if (this.clearButtons.length) {
			this.clearButtons.forEach(button => {
				this.clearForm(button);
				button.disabled = true;
			});
		}
		const popup = document.querySelector('#filter-popup');
		const selects = filter.querySelectorAll('[data-select]');
		if (popup) {
			const popupSelects = popup.querySelectorAll('[data-select]');
			if (popupSelects.length) {
				popupSelects.forEach(select => {
					this.createTag(select);
				});
			}
			const popupCheckboxes = popup.querySelectorAll('input.checkbox__input');
			if (popupCheckboxes.length) {
				popupCheckboxes.forEach(input => {
					this.createTag(input);
				});
			}
		}
		if (selects.length) {
			selects.forEach(select => {
				this.createTag(select);
			});
		}
		const checkboxes = filter.querySelectorAll('input.checkbox__input');
		if (checkboxes.length) {
			checkboxes.forEach(input => {
				this.createTag(input);
			});
		}
	}

	clearForm(button) {
		const form = button.closest('form');
		if (form) {
			button.addEventListener('click', (e) => {
				e.preventDefault();
				//form.reset();
				const checkboxes = document.querySelectorAll('form .checkbox__input, #filter-popup .checkbox__input');
				if (checkboxes.length) {
					checkboxes.forEach(checkbox => {
						checkbox.checked = false;
					});
				}
				const temporalTags = form.querySelectorAll('.filter-tags__el--temporal');
				if (temporalTags.length) {
					temporalTags.forEach(tag => {
						this.tagWrap.removeChild(tag);
					});
				}
				button.classList.remove('active');
				const selects = document.querySelectorAll('form [data-select], #filter-popup [data-select]');
				if (selects.length) {
					selects.forEach(select => {
						const $select = $(select);
						$select.val(null)
							.trigger('change');
					});
				}
				this.tempValues = [];
				if (this.clearButtons.length) {
					this.clearButtons.forEach(button => {
						button.disabled = true;
					});
				}
			});
		}
	}

	createTag(el) {
		if (el.classList.contains('select')) {
			const $select = $(el);
			if ($select.closest('.filter-result__sort').length > 0) return;
			$select.on('change', () => {
				const selectName = $select[0].name;
				const selectArray = $select.select2('data');
				if (selectName && $select.attr('multiple')) {
					const values = $select.val();
					if ($select.closest('form').length > 0) {
						const $current = $(`#filter-popup [data-select][name='${selectName}']`);
						$current.val(values)
							.trigger('change');
					} else if ($select.closest('#modal-form').length > 0) {
						const $current = $(`form [data-select][name='${selectName}']`);
						$current.val(values)
							.trigger('change');
					}
					for (let j = 0; j < selectArray.length; j++) {
						const value = $select.select2('data')[j].element.value;
						const text = $select.select2('data')[j].text.trim();
						if (value === '' || !value) return;
						if (this.tempValues.indexOf(value) === -1) {
							this.tempValues.push(value);
							this.buildTag(selectName, value, $select, null, text);
						}
					}
					this.tempValues.forEach(val => {
						if (values.indexOf(val) === -1) {
							const current = this.tagWrap.querySelector(`[data-value='${val}']`);
							if (current && current.dataset.name === selectName) {
								this.tagWrap.removeChild(current);
								this.tempValues.splice(this.tempValues.indexOf(val), 1);
							}
						}
					});
				} else if (selectName && !$select.attr('multiple')) {
					const value = $select.select2('data')[0].element.value;
					const text = $select.select2('data')[0].text.trim();
					if (value === '' || !value) return;
					if (this.tempValues.indexOf(value) === -1) {
						if ($select.closest('form').length > 0) {
							const $current = $(`#filter-popup [data-select][name='${selectName}']`);
							$current.val(value)
								.trigger('change');
						} else if ($select.closest('#modal-form')) {
							const $current = $(`form [data-select][name='${selectName}']`);
							$current.val(value)
								.trigger('change');
						}
						this.tempValues.push(value);
						this.buildTag(selectName, value, $select, null, text);
					}
				}
				if (this.tempValues.length > 0 && this.clearButtons.length) {
					this.clearButtons.forEach(button => {
						button.disabled = false;
					});
				} else if (this.clearButtons.length) {
					this.clearButtons.forEach(button => {
						button.disabled = true;
					});
				}
			});
		} else if (el.classList.contains('checkbox__input')) {
			const $input = $(el);
			const inputName = $input[0].name;
			const value = $input[0].value;
			$input.attr('data-value', value);
			if ($input.prop('checked') === true) {
				if (inputName && value) {
					if (this.clearButtons.length) {
						this.clearButtons.forEach(button => {
							button.disabled = false;
						});
					}
					if (value === '' || !value) return;
					if (this.tempValues.indexOf(value) === -1) {
						this.tempValues.push(value);
						this.buildTag(inputName, value, null, $input);
					}
				}
			}
			$input.on('change', () => {
				if (inputName && value) {
					if (value === '' || !value) return;
					if (this.tempValues.indexOf(value) === -1) {
						const $inputs = $(`.checkbox__input[data-value='${value}']`);
						$inputs.each((index, item) => {
							const $current = $(item);
							$current.prop('checked', true);
						});
						this.tempValues.push(value);
						this.buildTag(inputName, value, null, $input);
					}
					if (this.tempValues.indexOf(value) !== -1 && $input.prop('checked') === false) {
						const $inputs = $(`.checkbox__input[data-value='${value}']`);
						$inputs.each((index, item) => {
							const $current = $(item);
							$current.prop('checked', false);
						});
						const current = this.tagWrap.querySelector(`[data-value='${value}']`);
						this.tagWrap.removeChild(current);
						this.tempValues.splice(this.tempValues.indexOf(value), 1);
					}
				}
				if (this.tempValues.length > 0 && this.clearButtons.length) {
					this.clearButtons.forEach(button => {
						button.disabled = false;
					});
				} else if (this.clearButtons.length) {
					this.clearButtons.forEach(button => {
						button.disabled = true;
					});
				}
			});
		}
	}

	buildTag(name, value, select, input, text) {
		const tag = document.createElement('div');
		const tagText = text ? text : value;
		tag.classList.add('filter-tags__el', 'filter-tags__el--temporal');
		tag.dataset.value = value;
		tag.dataset.name = name;
		tag.innerHTML = `<span class="filter-tags__el-inner" title="${tagText}">
								<span class="filter-tags__el-text">${tagText}</span>
								<button data-tag-delete>
								<i class="icon is-default link-action__icon" aria-hidden="true"><svg viewBox="0 0 24 24" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
    							<path d="m18.7 17.3c.4.4.4 1 0 1.4-.2.2-.4.3-.7.3s-.5-.1-.7-.3l-5.3-5.3-5.3 5.3c-.2.2-.4.3-.7.3s-.5-.1-.7-.3c-.4-.4-.4-1 0-1.4l5.3-5.3-5.3-5.3c-.4-.4-.4-1 0-1.4s1-.4 1.4 0l5.3 5.3 5.3-5.3c.4-.4 1-.4 1.4 0s.4 1 0 1.4l-5.3 5.3z"></path>
								</svg>
								</i>
								</button>
								</span>
								<input type="hidden" name="${name}[${value}]" value="${tagText}">`;
		const close = tag.querySelector('[data-tag-delete]');
		if (select) {
			close.addEventListener('click', (e) => {
				e.preventDefault();
				const current = this.tagWrap.querySelector(`[data-value='${value}']`);
				this.tagWrap.removeChild(current);
				this.tempValues.splice(this.tempValues.indexOf(value), 1);
				const $selects = $(`[data-select][name='${name}']`);
				$selects.each((index, item) => {
					const $current = $(item);
					const multiple = $current.attr('multiple');
					let values = $current.val();
					if (values && multiple) {
						const i = values.indexOf(value);
						if (i >= 0) {
							values.splice(i, 1);
							$current.val(values)
								.trigger('change');
						}
					} else if (values) {
						$current.val(null)
							.trigger('change');
					}
				});
				if (this.tempValues.length === 0 && this.clearButtons.length) {
					this.clearButtons.forEach(button => {
						button.disabled = true;
					});
				}
			});
		} else if (input) {
			close.addEventListener('click', (e) => {
				const $input = $(`.checkbox__input[data-value='${value}']`);
				e.preventDefault();
				const current = this.tagWrap.querySelector(`[data-value='${value}']`);
				this.tagWrap.removeChild(current);
				this.tempValues.splice(this.tempValues.indexOf(value), 1);
				$input.each((index, item) => {
					const $current = $(item);
					$current.prop('checked', false);
				});
				if (this.tempValues.length === 0 && this.clearButtons.length) {
					this.clearButtons.forEach(button => {
						button.disabled = true;
					});
				}
			});
		}
		this.tagWrap.appendChild(tag);
	}
}
