import { isDesktop, isMob } from '../../utils/breakpoints';
import transitionDelayDefinition from '../../utils/transitionDealy';

const header = {
	init() {
		const header = document.querySelector('nav.main-side.menu');
		if (header) {
			this.header = header;
			this.headerContent = header.querySelector('.menu-content');
			this.headerMain = header.querySelector('.menu-content__main');
			this.hamburger = header.querySelector('.hamburger');
			this.hamburgerWrap = this.hamburger.closest('.menu-bar__hamburger');
			this.primaryNav = header.querySelector('.menu-content__primary-nav.primary-nav');
			this.primarySubNav = header.querySelectorAll('.menu-content__primary-sub-nav.primary-sub-nav');
			this.secondaryNav = header.querySelector('.menu-content__secondary-nav.secondary-nav');
			this.supportsTouch = ('ontouchstart' in document.documentElement);
			this.timer = 200;
			this.TOGGLE_CLASS = 'menu--open';
			this.ACTIVE_CLASS = 'active';
			this.HIDDEN_CLASS = 'hidden';
			this.timer = null;

			this.eventDefinition();
			this.showMenuDelayDefinition();
			this.hamburgerInit(this.hamburger);
			this.menuToggleDesktop();
			this.menuToggleMob();
			this.resizeListeners();
			this.listeners();

			if (isDesktop()) {
				this.header.dispatchEvent(new CustomEvent('desktopToggleInit'));
			}

			transitionDelayDefinition.init();
		}
	},

	eventDefinition() {
		if (this.supportsTouch) {
			this.event = 'click';
		} else {
			this.event = 'mouseenter';
		}
	},

	showMenuDelayDefinition() {
		if (this.supportsTouch) {
			this.showMenuDelay = 0;
		} else {
			this.showMenuDelay = 300;
		}
	},

	hamburgerInit(hamburger) {
		if (!hamburger) return;
		const that = this;
		let bgDark = false;
		const hamburgerText = hamburger.querySelector('.hamburger__text');
		const hamburgerCurrentText = hamburgerText.innerText.trim();
		const hamburgerCloseText = hamburgerText.dataset.closeText || hamburger.innerText.trim();

		const changeBackgroundMob = () => {
			if (that.headerContent.scrollTop >= 10) {
				that.hamburgerWrap.classList.add('menu-bar__hamburger--dark');
			} else {
				that.hamburgerWrap.classList.remove('menu-bar__hamburger--dark');
			}
		};
		hamburger.addEventListener('click', () => {
			if (!this.header.classList.contains(this.TOGGLE_CLASS)) {
				transitionDelayDefinition.definition();
				this.header.classList.add(this.TOGGLE_CLASS);
				this.secondaryNav.classList.remove(this.HIDDEN_CLASS);
				hamburgerText.innerText = hamburgerCloseText;
				if (isMob()) {
					if (this.headerContent.scrollTop <= 10) {
						this.hamburgerWrap.classList.remove('menu-bar__hamburger--dark');
					} else {
						this.hamburgerWrap.classList.add('menu-bar__hamburger--dark');
					}
					this.headerContent.addEventListener('scroll', changeBackgroundMob);
				}
				setTimeout(() => {
					window._disableScroll();
				}, 500);
			} else {
				window._enableScroll();
				this.header.classList.remove(this.TOGGLE_CLASS);
				this.closeMenu();
				this.secondaryNav.classList.add(this.HIDDEN_CLASS);
				hamburgerText.innerText = hamburgerCurrentText;
				transitionDelayDefinition.destroy();
				if (isMob()) {
					if (bgDark) {
						this.hamburgerWrap.classList.add('menu-bar__hamburger--dark');
						bgDark = false;
					} else {
						this.hamburgerWrap.classList.remove('menu-bar__hamburger--dark');
					}
					this.headerContent.removeEventListener('scroll', changeBackgroundMob);
				}
			}
		});

		if (isMob()) {
			const hamburgerChangeBackgroundTriggers = document.querySelectorAll('.bg-white');
			if (hamburgerChangeBackgroundTriggers.length > 0) {
				const rootMargin = window.innerHeight;
				let rootMarginOptions = '';

				if (rootMargin > 0) {
					rootMarginOptions = `0px 0px -${rootMargin}px 0px`;
				} else {
					rootMarginOptions = `0px 0px ${rootMargin}px 0px`;
				}

				const obs = new IntersectionObserver((entries, observer) => {
					entries.forEach((entry) => {
						if (entry.isIntersecting) {
							window.dispatchEvent(new CustomEvent('hamburgerBackgroundChange'));
							bgDark = true;
						} else {
							window.dispatchEvent(new CustomEvent('hamburgerBackgroundRevert'));
							bgDark = false;
						}
					});
				}, {
					rootMargin: rootMarginOptions,
				});

				hamburgerChangeBackgroundTriggers.forEach((trigger) => {
					obs.observe(trigger);
				});

				window.addEventListener('hamburgerBackgroundChange', () => {
					if (isMob()) {
						this.hamburgerWrap.classList.add('menu-bar__hamburger--dark');
					}
				});
				window.addEventListener('hamburgerBackgroundRevert', () => {
					if (isMob()) {
						this.hamburgerWrap.classList.remove('menu-bar__hamburger--dark');
					}
				});
			}
		}
	},

	menuToggleDesktop() {
		if (!this.primaryNav) return;
		const navItems = this.primaryNav.querySelectorAll('.primary-nav__item');
		const resetActive = () => {
			navItems.forEach((item) => {
				item.classList.remove(this.ACTIVE_CLASS);
				this.primaryNav.classList.remove(this.ACTIVE_CLASS);
			});
		};
		const desktopToggle = (item, e) => {
			const currentItem = item;
			this.primaryNav.classList.add(this.ACTIVE_CLASS);
			if (item.hasAttribute('data-submenu')) {
				if (this.supportsTouch && !item.classList.contains(this.ACTIVE_CLASS)) {
					e.preventDefault();
				}
				this.timer = setTimeout(() => {
					item.classList.add(this.ACTIVE_CLASS);
					const subMenu = item.dataset.submenu;
					let current;
					navItems.forEach((item) => {
						if (item !== currentItem) {
							item.classList.remove(this.ACTIVE_CLASS);
						}
					});
					this.primarySubNav.forEach((sub) => {
						if (sub.dataset.submenu === subMenu) {
							transitionDelayDefinition.definition(sub);
							if (sub.hasAttribute('data-submenu-long')) {
								this.headerMain.classList.add('menu-content__main--long-submenu');
								this.secondaryNav.classList.add(this.HIDDEN_CLASS);
							}
							sub.classList.remove(this.HIDDEN_CLASS);
							current = sub;
						}
					});
					this.primarySubNav.forEach((itemsub) => {
						if (itemsub !== current) {
							transitionDelayDefinition.destroy(itemsub);
							itemsub.classList.add(this.HIDDEN_CLASS);
						}
					});
				}, this.showMenuDelay);
			} else {
				this.timer = setTimeout(() => {
					currentItem.classList.add(this.ACTIVE_CLASS);
					navItems.forEach((item) => {
						if (item !== currentItem) {
							item.classList.remove(this.ACTIVE_CLASS);
						}
					});
					this.primarySubNav.forEach((itemsub) => {
						transitionDelayDefinition.destroy(itemsub);
						itemsub.classList.add(this.HIDDEN_CLASS);
						this.headerMain.classList.remove('menu-content__main--long-submenu');
						this.secondaryNav.classList.remove(this.HIDDEN_CLASS);
					});
				}, this.showMenuDelay);
			}
		};
		if (navItems.length) {
			navItems.forEach((item) => {
				const handler = desktopToggle.bind(null, item);
				this.header.addEventListener('desktopToggleInit', () => {
					item.addEventListener(this.event, handler);
					item.addEventListener('mouseout', () => {
						if (this.timer) {
							clearTimeout(this.timer);
						}
					})
				});
				this.header.addEventListener('desktopToggleDestroy', () => {
					item.removeEventListener(this.event, handler);
				});
			});
		}
	},

	menuToggleMob() {
		if (!this.primaryNav) return;
		const navItems = this.primaryNav.querySelectorAll('.primary-nav__item');
		if (navItems.length) {
			navItems.forEach((item) => {
				const buttonToggle = item.querySelector('.primary-nav__item-toggle-button');
				if (buttonToggle) {
					buttonToggle.addEventListener('click', () => {
						if (item.hasAttribute('data-submenu')) {
							this.primaryNav.classList.add(this.HIDDEN_CLASS);
							const subMenu = item.dataset.submenu;
							let current;
							this.primarySubNav.forEach((sub) => {
								if (sub.dataset.submenu === subMenu) {
									transitionDelayDefinition.definition(sub);
									sub.classList.remove(this.HIDDEN_CLASS);
									current = sub;
								}
							});
							this.primarySubNav.forEach((itemsub) => {
								if (itemsub !== current) {
									transitionDelayDefinition.destroy(itemsub);
									itemsub.classList.add(this.HIDDEN_CLASS);
								}
							});
						}
					});
				}
			});
		}
		const backwardButtons = this.header.querySelectorAll('.primary-sub-nav__backward-button');
		if (backwardButtons.length) {
			backwardButtons.forEach((button) => {
				button.addEventListener('click', () => {
					this.primaryNav.classList.remove(this.HIDDEN_CLASS);
					const currentSubMenu = button.closest('[data-submenu]');
					transitionDelayDefinition.destroy(currentSubMenu);
					currentSubMenu.classList.add(this.HIDDEN_CLASS);
				});
			});
		}
	},

	closeMenu() {
		const activeItems = this.header.querySelectorAll(`.${this.ACTIVE_CLASS}`);
		if (activeItems.length) {
			activeItems.forEach((item) => {
				item.classList.remove(this.ACTIVE_CLASS);
				if (this.primaryNav.classList.contains(this.HIDDEN_CLASS)) {
					this.primaryNav.classList.remove(this.HIDDEN_CLASS);
				}
			});
		}
		this.primarySubNav.forEach((sub) => {
			sub.classList.add(this.HIDDEN_CLASS);
		});
		if (this.primaryNav.classList.contains(this.HIDDEN_CLASS)) {
			this.primaryNav.classList.remove(this.HIDDEN_CLASS);
		}
		this.headerMain.classList.remove('menu-content__main--long-submenu');
		const transitionDelayItems = this.header.querySelectorAll('[data-delay-children]');
		if (transitionDelayItems.length) {
			transitionDelayItems.forEach((item) => {
				item.style.transitionDelay = '';
			});
		}
	},

	resizeListeners() {
		let lastWidth = window.innerWidth;
		window.addEventListener('resize', window._throttle(() => {
			if (lastWidth < 990 && window.innerWidth > 989) {
				this.header.dispatchEvent(new CustomEvent('desktopToggleInit'));
				if (this.primaryNav.classList.contains(this.HIDDEN_CLASS)) {
					this.primaryNav.classList.remove(this.HIDDEN_CLASS);
				}
				this.primarySubNav.forEach((itemsub) => {
					if (!itemsub.classList.contains(this.HIDDEN_CLASS) && itemsub.hasAttribute('data-submenu-long')) {
						this.headerMain.classList.add('menu-content__main--long-submenu');
						this.secondaryNav.classList.add('hidden');
					}
				});
			} else if (lastWidth > 989 && window.innerWidth < 990) {
				this.header.dispatchEvent(new CustomEvent('desktopToggleDestroy'));
				this.primarySubNav.forEach((itemsub) => {
					if (!itemsub.classList.contains(this.HIDDEN_CLASS)) {
						if (itemsub.hasAttribute('data-submenu-long')) {
							this.headerMain.classList.remove('menu-content__main--long-submenu');
							this.secondaryNav.classList.remove('hidden');
							this.primaryNav.classList.add(this.HIDDEN_CLASS);
						} else {
							this.primaryNav.classList.add(this.HIDDEN_CLASS);
						}
					}
				});
			} else if (lastWidth < 640 && window.innerWidth > 639) {
				this.hamburgerWrap.classList.remove('menu-bar__hamburger--dark');
			}
			lastWidth = window.innerWidth;
		}, 300), {
			passive: true,
		});
	},

	listeners() {
		window.addEventListener('modal:open', () => {
			if (this.header.classList.contains('menu--open')) {
				window._enableScroll();
				this.header.classList.remove(this.TOGGLE_CLASS);
				this.closeMenu();
			}
		});
	},
};

export default header;
