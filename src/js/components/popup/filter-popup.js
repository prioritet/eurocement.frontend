import 'magnific-popup/dist/jquery.magnific-popup.min';
import $ from 'jquery';

document.addEventListener('DOMContentLoaded', () => {
	$('[data-ext-filter]').magnificPopup({
		type:'inline',
		midClick: false,
		removalDelay: 300,
		mainClass: 'mfp-fade mfp-filter-popup',
		showCloseBtn: false,
		fixedContentPos: true,
	});

	// Open
	$('[data-ext-filter]').click(function(){
		$(this).magnificPopup('open');
	});

	//Close
	$('.filter-popup__close').click(function(){
		$.magnificPopup.close();
	});

});
