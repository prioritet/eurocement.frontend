import tippy from 'tippy.js';

window.addEventListener('init.tooltip', () => {
    const isIOS = /iphone|ipad/gi.test(navigator.userAgent);

    let trigger = '';
    if (isIOS) {
        trigger = 'click';
    } else {
        trigger = 'mouseenter focus';
    }

    tippy('[data-tippy-content]', {
        boundary: 'window',
        animation: 'shift-toward',
		arrow: false,
        trigger,
		maxWidth: '13rem',
    });
});

window.dispatchEvent(new CustomEvent('init.tooltip'));
