# Image
```
image: {
    // Тип изображения, влияет на то, каким образом оно вставится (img или background-image)
    // Предпочтительнее ничего не указывать, чтобы изображение вставлялось как img
    type: 'bg',

    src: 'path/to/image',
    srcset: 'path/to/retina 2x', // не работает, если type == 'bg'
    alt: 'alternative text',

    // дополнительный класс обертке
    // доступные для image: is-cover, is-contain
    class: 'is-cover some-other-class',

    // Ленивая загрузка
    disable_lazy: false, // если false, то src становится data-src, а в src вставляет изображение-заглушку
}
```
