# select
```
{
    "select": true,
    "name": "CATEGORY_TYPE",
    "label": "Выберите рубрику",    // текст (label) перед селектом
    "placeholder": "Сортировка",    // текст по-умолчанию, когда ничего не выбрано
    "deselect_option": false",      // запрещает возможность снятия выбора с активного пункта (только выбор другого)
    "reset": false,                 // кнопка reset
    "search": false,                // поле поиска в выпадающем списке
    "sort": false,                  // сортировка элементов
    "required": false,              // обязательный для заполнения
    "disabled": false,              // выбор отключен
    "options": [
        {
            "value": "default",     // значение
            "label": "Все рубрики", // текст
            "selected": true        // активный пункт по умолчанию
        },
        {
            "value": "opt2",
            "label": "Технологии",
            "disabled": true        // пункт отключен
        }
    ]
}
```
